instance creation
newForError: aString issuedBy: anObject withinContext: context

	^ self new forError: aString issuedBy: anObject withinContext: context.